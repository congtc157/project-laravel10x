@extends('dashboard.index')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-tools float-left">
                    <form action="{{ route('products.index')}}" class="search-form" id="index" data-url="{{route('products.list')}}" method="POST">
                        @csrf
                        <div class="input-group input-group-outline mt-3">
                            <label class="form-label">Name</label>
                            <input type="text" class="form-control name" name="name" value="{{ request('name') }}">
                        </div>
                        <div class="input-group input-group-outline mt-3">
                        <select class="role form-control category" name="category" id="category">
                            <option value="">Select Category...</option>
                            @foreach ($categories as $category)
                              <option value="{{ $category->id }}">{{ $category->name }}</option> 
                            @endforeach
                        </select>  
                        </div>  
                        <button type="button" class="btn btn-primary btn-search">Search
                          <i class="fas fa-search"></i>
                        </button>
                    </form>
                  </div> 
                  @hasPermission('products.create', 'products.store')
                  <div class="card my-4">
                    <div class=" me-3 my-3 text-end">
                        <button type="button" class="btn bg-gradient-dark mb-0 product-action" data-bs-toggle="modal" data-bs-target="#exampleModal" data-url="{{ route('products.store')}}" data-action-name="create"><i
                            class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New
                        Product</button>
                    </div>
                    @endhasPermission
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        #</th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Name</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Price</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Categories</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Image</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('clients.products.form')
    </div>
    <ul class="pagination">  

    </ul>
    <x-footers.auth></x-footers.auth>
</div>
@endsection