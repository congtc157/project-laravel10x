<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Products</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form data-url="" data-action-name="" method="POST" class="product-data form-product-data">
              @csrf
              <div class="row">     
                  <div class="col-6">
                      <div class="mb-3">
                          <label class="form-label">Name</label>
                          <input type="text" name="name" class="form-control product-data input border border-2 p-2 name-product" >                    
                          <p class='name-error text-danger inputerror error'></p>                       
                      </div>
                      <div class="mb-3">
                          <label class="form-label">Category</label>
                          <select name="categories[]" class="form-control product-data select border border-2 p-2 select-categories" multiple>
                          </select>
                          <p class='categories-error text-danger inputerror error'></p>
                      </div>                  
                      <div class="mb-3">
                          <label class="form-label">Price</label>
                          <input type="text" name="price" class="form-control product-data input border border-2 p-2 price-product">
                          <p class='price-error text-danger inputerror error'></p>
                      </div>
                      <div class="mb-3">
                          <label class="form-label">Expiry</label>
                          <input type="date" name="expiry" class="form-control product-data input border border-2 p-2 expiry-product">
                          <p class='expiry-error text-danger inputerror error'></p>
                      </div>
                  </div> 
                  <div class="col-6">
                      <div class="mb-3 image-input">
                          <label class="form-label">Image</label>
                          <input type="file" name="image" class="form-control product-data input border border-2 p-2 input-image">
                          <p class='image-error text-danger inputerror error'></p>
                      </div>
                      <div class="mb-3 row">
                          <div class="col-6 n-image">
                              <label class="form-label">New Image</label>
                              <img class="image-preview" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png" alt="" width="100%">
                          </div>
                          <div class="col-6 hidden-image">
                              <label class="form-label">Old Image</label>
                              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png" class="o-image" alt="" width="100%">
                          </div>
                      </div>
                      <div class="mb-3">
                          <label class="form-label">Description</label>
                          <textarea name="description" class="form-control product-data textarea border border-2 p-2 description-product" rows="5"> </textarea>
                          <p class='description-error text-danger inputerror error'></p>
                      </div>
                  </div>
          </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" id="btn-close" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary btn-save">Save</button>
        </div>
      </div>
    </div>
  </div>
  