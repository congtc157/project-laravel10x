<?php

namespace App\Http\Requests\Users;

use App\Models\User;
use App\Rules\EmailDehaRule;
use App\Rules\VietNamPhoneNumberRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email',  new EmailDehaRule , 'unique:'.User::class],
            'phone' => ['required', new VietNamPhoneNumberRule],
            'password' => ['required', 'string', 'min:8'],
        ];
    }
}
