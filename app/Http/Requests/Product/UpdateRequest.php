<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'min:2'],
            'price' => ['required', 'numeric'],
            'description' => ['required', 'string'],
            'expiry' => ['required', 'date'],
            'image' => ['mimes:jpg,bmp,png', 'max:5000'],
            'categories' => ['array'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name không được để trống',
            'price.required' => 'Price không được để trống',
            'price.numeric' => 'Price phải là chữ số',
            'description.required' => 'Description không được để trống',
            'description.string' => 'Description không phải là số',
            'expiry.required' => 'Expiry không được để trống',
            'expiry.date' => 'Expiry sai định dạng',
            'image.mimes' => 'Sai định dạng Image',
            'image.max' => 'Quá dung lượng ảnh',
        ];
    }
}
