<?php
namespace App\Repositories;

use App\Models\Category;
use App\Models\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    public function search($data)
    {
        return $this->model->withName($data['name'])
        ->withCategory($data['category'])
        ->latest('id')
        ->paginate(10);
    }
}